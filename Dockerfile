FROM ubuntu:eoan

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get install -y --no-install-recommends \
    awscli \
    ca-certificates \
    curl \
    g++ \
    gcc \
    git \
    libevent-dev \
    libgmp-dev \
    libpcre3-dev \
    libreadline-dev \
    libssl-dev \
    libxml2-dev \
    libyaml-dev \
    libz-dev \
    make \
    pkg-config \
    postgresql-client \
    python-minimal \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

RUN curl -sSL https://github.com/crystal-lang/crystal/releases/download/0.33.0/crystal_0.33.0-1_amd64.deb -o /tmp/crystal.deb \
 && curl -sSL https://deb.nodesource.com/node_12.x/pool/main/n/nodejs/nodejs_12.16.1-1nodesource1_amd64.deb -o /tmp/node.deb \
 && curl -sSL https://github.com/yarnpkg/yarn/releases/download/v1.22.4/yarn_1.22.4_all.deb -o /tmp/yarn.deb \
 && curl -sSL https://github.com/watchexec/watchexec/releases/download/1.12.0/watchexec-1.12.0-x86_64-unknown-linux-gnu.deb -o /tmp/watchexec.deb \
 && dpkg -i /tmp/*.deb \
 && rm -rf /tmp/*.deb

RUN curl -sSL https://github.com/crystal-ameba/ameba/archive/v0.11.0.tar.gz | tar zx -C /tmp \
 && cd /tmp/ameba* \
 && CRFLAGS=--production make install \
 && curl -sSL https://gitlab.com/mgiken/mig/-/archive/v0.1.2/mig-v0.1.2.tar.gz | tar zx -C /tmp \
 && cd /tmp/mig* \
 && shards build --production \
 && cp bin/mig /usr/local/bin/mig \
 && rm -rf /root/.cache /tmp/*
